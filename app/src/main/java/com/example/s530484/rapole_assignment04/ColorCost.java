package com.example.s530484.rapole_assignment04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ColorCost extends AppCompatActivity {
    MainActivity m=new MainActivity();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_cost);
    }

    public void premium(View v)
    {
        Intent inti=new Intent();
        inti.putExtra("costpergallon",70.0);
        setResult(33,inti);
        finish();
    }
    public void standard(View v)
    {
        Intent inti=new Intent();
        inti.putExtra("costpergallon",50.0);
        setResult(33,inti);
        finish();
    }
    public void economy(View v)
    {
        Intent inti=new Intent();
        inti.putExtra("costpergallon",30.0);
        setResult(33,inti);
        finish();
    }
}
