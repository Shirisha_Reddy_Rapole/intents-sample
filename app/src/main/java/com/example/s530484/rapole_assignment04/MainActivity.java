package com.example.s530484.rapole_assignment04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.InputMismatchException;

public class MainActivity extends AppCompatActivity {
    TextView ans;
    private double area = 0.0;
    private double cost = 0.0;
    private double total = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void reset(View v) {
        area = 0.0;
        cost = 0.0;
        total=0.0;
        ans.setText("");
    }

    public void wallFunction(View v) {
        Intent init = new Intent(this,WallEntry.class);
        startActivityForResult(init, 07);
    }

    public void colorFunction(View v) {

        Intent init = new Intent(this, ColorCost.class);
        startActivityForResult(init, 19);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 07 && resultCode == 123 && data != null) {
            area = data.getDoubleExtra("area", 0.0);
        } else if (requestCode == 19 && resultCode == 33 && data != null) {
            ans = findViewById(R.id.res);
            cost = data.getDoubleExtra("costpergallon", 0.0);
        }
        ans = findViewById(R.id.res);
        total = (area/400) * cost;
        ans.setText("The cost is: $" + total + "");
    }
    public void layer(View v)
    {
        ans=(TextView) findViewById(R.id.res);
        try {
            if (((ToggleButton) v).isChecked()) {
                ans.setText("The cost for double layer is: $" + 2 * total + "");
            } else {
                ans.setText("The cost for single layer is: $" + total + "");
            }
        }
        catch(InputMismatchException e)
        {
            ans.setText("Please enter the wall dimensions");
        }

    }

}
