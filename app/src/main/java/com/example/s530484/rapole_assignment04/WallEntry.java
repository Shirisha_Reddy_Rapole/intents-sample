package com.example.s530484.rapole_assignment04;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class WallEntry extends AppCompatActivity {
    MainActivity m=new MainActivity();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wall_entry);
    }

    public void compute(View v)
    {

        EditText height = findViewById(R.id.ht);
        EditText width = findViewById(R.id.wt);
        EditText len = findViewById(R.id.lt);
        EditText walls = findViewById(R.id.num);

        if(height.length()>0&&width.length()>0&&len.length()>0) {
            String x = height.getText().toString();
            String y = width.getText().toString();
            String z = len.getText().toString();
            String wa = walls.getText().toString();
            double h = Double.parseDouble(x);
            double w = Double.parseDouble(y);
            double l= Double.parseDouble(z);
            double n = Double.parseDouble(wa);
            double area = n*(2*(l*w)+2*(l*h)+2*(w*h));
            Intent init = new Intent();
            init.putExtra("area", area);
            setResult(123, init);
            finish();
        }

    }
}
